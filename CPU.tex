\section{CPU Side}\label{sec:CPU}

\subsection{Finite Field Arithmetic Library}
A fully functioning finite field library for \GF{64} field that satisfies all needs of the FFT algorithms.

\subsubsection{Element Representation on CPU}
The library represents elements in \GF{64} as objects with an 8-byte (64-bit) value saved in a single 8-byte sized attribute.
Given an element $e$ , we will denote this 8-byte attribute as $c_e$.
We will also denote by $c_e^i$ the value of i\textsuperscript{th} least significant bit in $c_e$ with $c_e^0$ being the least significant bit.

A \GF{64} field can be defined by an irreducible polynomial of degree 64, that will be denoted by \irred
Each element in \GF{64} is known to be equivalent to a unique polynomial modulo \irred.
Each element $e$ will represent the polynomial
\[ p_e(x) = \sum_{i=0}^{63}{c_e^ix^i} \]

\subsubsection{Finite Field Library API}
The library's API consists of the following basic operations:
\begin{description}
	\item[Addition] Given two elements $a,b$ the representation of $a+b$ is $c_a \oplus c_b$.
	\item[Multiplication] See ~\ref{subsubsec:cpu_mult} for full description of this operation.
	\item[Squaring] Implemented as a multiplication of an element by itself.
	\item[Exponentiation] Implemented using the repetitive squaring and multiplying algorithm.
	\item[Inversion] The inverse of an element $a$ is an element $b$ s.t. there exists a polynomial $q(x)$ for which
	$a(x)\cdot b(x) + q(x)\cdot r(x) = 1$.The inversion is implemented by implementing the extended euclidean algorithm to find this $b$.
\end{description}



\subsubsection{Fast Multiplication in \GF{n}}
\label{subsubsec:cpu_mult}

Finite field multiplication is generally far more time consuming than addition, both in terms of bit operations and in terms of machine cycles, when turning into finite field software implementation. This particularly holds for the fields we are interested in, finite fields of characteristic $2$.

Multiplication speed in \GF{n} is tightly connected to the field representation (addition is XOR under any basis for \GF{n} over \GF{}). The two basic representations are,
\begin{enumerate}
  \item Standard Basis: Elements are polynomials in $\GF{}[X]$ and multiplication is carried out modulo an irreducible polynomial of degree $n$ over $\GF{}[X]$.
  \item Normal Basis: Elements are the Frobenius automorphisms of a basic element $\alpha$ and multiplication is defined by a matrix. See \cite{Gao93normalbases} for additional details.
\end{enumerate}
We work under the standard basis. To speed up multiplication we choose a special kind of irreducible polynomial, called a {\em 2-gapped} polynomial. We show that multiplication in \GF{n} can be reduced to one multiplication of polynomials of degree n-1 and two multiplications of polynomials of degree $ \frac{n}{2}$ over the ring $\GF{}[X]$ and \(2n\) additions in \GF{}.


\begin{definition}
	An irreducible polynomial $\irred = \sum{a_ix^i} $  of degree d is \textbf{k-\irredProperty} if it can be written as $\irred = x^d - r_1(x)$ where $\deg{(r_1(x))} \leq \sfrac{d}{k}$
\end{definition}

Let us first introduce the notion and importance of $k-\irredProperty$ polynomials  in \GF{n} field for $k=2$, as described in \autoref{alg:mul2gapn}.



\begin{algorithm}
	\caption{2-\irredProperty Multiplication in \GF{n}}
	\label{alg:mul2gapn}
	\begin{algorithmic}[1]
		\Require
		\Statex \begin{itemize}
			\item $a(x),b(x)$ of degree at most \(n-1\) in \PolyRing.
			\item $\irred=x^{n}-r_1(x)$, 2-Gapped polynomial in \PolyRing of degree
			\(n\).
		\end{itemize}
		\Ensure $h(x) = (a(x)*b(x)) \mod{\irred}$
		\State $h(x) \leftarrow a(x)*b(x)$
		\State $h(x) \leftarrow  h_0^{\sfrac{3n}{2}-1}(x) + h_{\sfrac{3n}{2}}^{2n-1}(x)*r_1(x)* x^{\sfrac{n}{2}}$
		\State $h(x) \leftarrow  h_0^{n-1}(x) + h_{n}^{\sfrac{3n}{2}}(x)*r_1(x)$
		\State \Return $h(x)$
	\end{algorithmic}
\end{algorithm}


Denote by $h_i(x)$ the value of $h(x)$ calculated on step $i$.
\begin{lemma}
	\label{lem:mul641}
	$h_1(x)\equiv h_2(x) \mod{\irred}$
\end{lemma}

\begin{proof}	
	\begin{alignat*}{3}
		a(x)\cdot b(x)  &= h_1(x) \\
		&= h_1^0(x) + x^{\sfrac{3n}{2}}\cdot h_1^1(x) && \qquad\deg\left(h_1^i(x)\right) \leq \sfrac{n}{2}-1 \\
		&\equiv h_1^0(x)+ r_1(x)\cdot h_1^1(x)\cdot x^{\sfrac{n}{2}} \mod{\irred} && \qquad x^{n}\equiv r_1(x) \mod{\irred} \\
		&= h_2(x) \mod{\irred}
	\end{alignat*}
\end{proof}

\begin{lemma}
	\label{lem:mul642}
	$\deg{\left(h_2(x)\right)}\leq \sfrac{3n}{2}-1$
\end{lemma}

\begin{proof}
	\begin{alignat*}{3}
		\deg{\left(h_2(x)\right)} &= \max{\left(\deg{\left(h_1^0(x)\right)},\deg{\left(x^{\sfrac{n}{2}}\right)}+\deg{\left(h_1^1(x)\right)}+\deg{\left(r_1(x)\right)}\right)}\\
		&\leq \max{\left(n-1,\sfrac{n}{2}+\sfrac{n}{2}-1+\sfrac{n}{2}\right)} \\
		&= \sfrac{3n}{2}-1
	\end{alignat*}
\end{proof}

\begin{lemma}
	\label{lem:mul643}
	$h_2(x)\equiv h_3(x) \mod{\irred}$
\end{lemma}
\begin{proof}
	\begin{alignat*}{3}
		h_2(x)  &= h_2^0(x) + x^{n}\cdot h_2^1(x) && \qquad\deg\left(h_2^0(x)\right) \leq n-1 \quad \deg\left(h_2^1(x)\right) \leq \sfrac{n}{2}-1  \\
		&\equiv h_2^0(x)+ r_1(x)\cdot h_2^1(x) \mod{\irred} && \qquad x^{n}\equiv r_1(x) \mod{\irred} \\
		&= h_3(x) \mod{\irred}
	\end{alignat*}
\end{proof}
\begin{lemma}
	\label{lem:mul644}
	$\deg{\left(h_3(x)\right)}\leq n-1$
\end{lemma}
\begin{proof}
	\begin{alignat*}{3}
		\deg{\left(h_3(x)\right)} &= \max{\left(\deg{\left(h_2^0(x)\right)},\deg{\left(h_2^1(x)\right)}+\deg{\left(r_1(x)\right)}\right)}\\
		&\leq \max{\left(n-1,\sfrac{n}{2}-1+\sfrac{n}{2}\right)} \\
		&= \sfrac{n}{2}-1
	\end{alignat*}
\end{proof}


\begin{lemma}
	\label{lem:645}
	$h_3(x) = \left(a(x)\cdot b(x) \mod{\irred}\right)$
\end{lemma}
\begin{proof}
	From \autoref{lem:mul641} and \autoref{lem:mul643} we get $h_3(x) \equiv a(x)\cdot b(x) \mod{\irred}$. From \autopageref{lem:mul644} we get that $\deg{\left(h_3(x)\right)} \leq n-1$ so the equality holds.
\end{proof}

This novel algorithm, minimizing number of polynomial multiplications, can be adapted to multiplication in our field of interest, \GF{64}.
We will focus on efficient software implementation of multiplication in that field as described in \autoref{alg:mul64}.

The \clmulName instruction, introduced in 2010 by intel and from then can be found in any modern CPU, multiplies two polynomials of degree at most 63 over \(\GF{}[X]\). This instruction is implemented in hardware and executed much faster than any software implementation with the same purpose. Therefore, this instruction will have a main role in our multiplication algorithm in \GF{64}

\begin{notation}
	Given a polynomial $p(x) = \sum{a_kx^k}$ we wil denote by $p_i^j(x) = \sum_{k=i}^j{a_kx^{k-i}} $
\end{notation}

\begin{algorithm}
	\caption{2-\irredProperty Multiplication in \GF{64} using \clmulName}
	\label{alg:mul64}
	\begin{algorithmic}[1]
		\Require
		\Statex \begin{itemize}
			\item $a(x),b(x)$ of degree at most 63 in \PolyRing.
			\item $\irred=x^{64}+r_1(x)$, 2-Gapped polynomial in \PolyRing of degree 64.
		\end{itemize}
		\Ensure $h(x) = (a(x)\cdot b(x)) \mod{\irred}$
		\State $h(x) \leftarrow \clmul{a(x)}{b(x)}$
		\State $h(x) \leftarrow  h_0^{95}(x) + \clmul{h_{96}^{127}(x)}{r_1(x)}\cdot x^{32}$
		\State $h(x) \leftarrow  h_0^{63}(x) + \clmul{h_{64}^{95}(x)}{r_1(x)}$
		\State \Return $h(x)$
	\end{algorithmic}
\end{algorithm}

Our C++ implementation of \autoref{alg:mul64} outperforms NTL's implementation of by approximately 7 \%. \matan{will add more informative results soon}


\subsubsection{Generalization for Optimized Multiplication in k-\irredProperty Finite Fields}

In \autoref{alg:mulgeneral} we also present an extension to this multiplication for general $k-Gapped$ fields \GFF{p}{m}.

\begin{algorithm}
	\caption{k-\irredProperty Multiplication in \GFF{p}{m}}
	\label{alg:mulgeneral}
	\begin{algorithmic}[1]
		\Require
		\Statex \begin{itemize}
			\item $a(x),b(x)$ of degree at most $m-1$ in \ring{p}.
			\item $\irred=x^{m}-r_1(x)$,  k-Gapped polynomial in \ring{p} of degree m.
		\end{itemize}
		\Ensure $h(x) = (a(x)\cdot b(x)) \mod{\irred}$
		\State $\ell \leftarrow \sfrac{m}{k}$
		\State $h(x) \leftarrow a(x)*b(x)$
		\For{$i=k-1$ down to $0$}
			\State $t\leftarrow m+\ell \cdot i$
			\State $h(x) \leftarrow h_0^{t-1}(x)+h_t^{t+\ell-1}(x)* r_1(x)* x^{t-m}$ \label{ln:mulsmallmul}
		\EndFor
		\State \Return $h(x)$
	\end{algorithmic}
\end{algorithm}

Let us denote by $A(n)$ and $M(n)$ as the numbers of additions and multiplications in \GFF{p}{} that performed when multiplying to polynomials of degree at most $n$ over the ring \(\GFF{p}{}[X]\).

	\begin{theorem}
		\autoref{alg:mulgeneral} performs,
		\begin{itemize}
			\item \(2m+A(m-1)+k\cdot A\left(\frac{m}{k}\right)\) additions in \GFF{p}{k}.
			\item \(M(m-1)+k\cdot M\left(\frac{m}{k}\right)\) multiplications in \GFF{p}{k}
		\end{itemize}
	\end{theorem}

\begin{proof}	We will count separately the number of operations within polynomials multiplications and out of them.
	\begin{itemize}
		\item The number of additions in \GFF{p}{k} which are not part of polynomial multiplications is at most \(2m\). In each iteration we add the polynomial \(h_t^{t+\ell-1}(x)* r_1(x)* x^{t-m} \) which has at most \(\frac{2m}{k}\) non-zero coefficients, which are the topmost coefficients, while others will be zero. This addition requires \(\frac{2m}{k}\) additions. Over $k$ iterations there will be $2m$ additions in total.
		\item There are no multiplications in \GFF{p}{} except for those which are part of polynomial multiplications.
		\item Let us denote by $A(n)$ and $M(n)$ the number of additions and multiplications in \GFF{p}{} needed to multiply two polynomials of degree $n$ over the ring \GFF{p}{}[X] respectively. Our algorithm first multiplies two polynomials of degree at most $m-1$ in and then multiplies $k$ times polynomials of degree $\frac{m}{k}$, all over the ring \( \GFF{p}{}[X]\). This takes $M(m-1)+k\cdot M\left(\frac{m}{k}\right)$ multiplications and $A(m-1)+k\cdot A\left(\frac{m}{k}\right)$ additions in \GFF{p}{k}
	\end{itemize}
	\end{proof}

	Notice that number of multiplications and additions depends on the algorithm that is used to multiply polynomials.
	Best known algorithm known is FFT-based and has $O(n\log n \log \log n)$ time complexity.
	However, in practice, Karatsuba's multiplication of polynomials tends to be faster than FFT-based multiplications.
	The use of the \clmulName instruction to implement Karatsuba's multiplication in higher order binary fields is explained in detail in \cite{Su:2012:IIN:2184922.2185040} and can be used to extend our algorithm into implementation for binary extension fields of higher degrees.


\begin{lemma}[Correctness]
	The algorithm outputs $\left(a(x) \cdot b(x)\right)\mod{\irred} $.
\end{lemma}
\begin{proof}
	Denote by $h_j(x)$ the value of $h(x)$ as computed after the iteration in which $i=j$ and $h_k(x)$ will be $h(x)$ before the loop. So $h_k(x) = a(x) \cdot b(x)$. We will prove by induction on $j$ that $h_j(x) \equiv \left(a(x)\cdot b(x)\right) \mod{\irred}$ and that the degree of $h_j(x)$ is at most $m+j\cdot \ell - 1$.
	
	In the base case, $j=k$, and $h_k(x) = a(x) \cdot b(x)$ so its' degree is at most $2m$ and the claim obviously holds.
	
	Assume the for some $n$ we know that the claim holds, now we shall prove it for $n-1$.
	Denote by $t$ the value of variable $t$ in this iteration where $t=m+\ell\cdot (n-1)$. According to step 5 of the algorithm $h_{n-1}(x) = \left(h_n\right)_0^{t-1}(x) + \left(h_n\right)_{t}^{t+\ell-1}(x)\cdot r_1(x) \cdot x^{t-m} $ so,
	\begin{alignat*}{2}
	\Deg{h_{n-1}(x)} &= \Deg{\left(h_n\right)_0^{t-1}(x) + \left(h_n\right)_{t}^{t+\ell-1}(x)\cdot r_1(x) \cdot x^{t-m}}\\
	&= \max{\left(\Deg{\left(h_n\right)_0^{t-1}(x)},\Deg{ \left(h_n\right)_{t}^{t+\ell-1}(x)\cdot r_1(x) \cdot x^{t-m}}\right)} \\
	&= \max{\left(t-1,\ell-1+\ell+t-m\right)}\\
	&= \otimes
	\end{alignat*}
	Since $k\geq 2$ then $\ell = \sfrac{m}{k} \leq \sfrac{m}{2}$ so,
	\begin{alignat*}{2}
		\otimes & \leq \max{\left(t-1,2\cdot\sfrac{m}{2} +t - m\right)} \\
		&= \max{\left(t-1,t-1\right)} \\
		&= t-1 \\
		&= m+ \ell \cdot (n-1) -1.
	\end{alignat*}
	
	Now we shall prove by induction that $h_i(x) \equiv h_k(x) \mod{\irred}$.
	Base is obvious for $k$.
	Assume that $h_n(x) \equiv h_k(x) \mod{\irred}$ , since $\Deg{h_n(x)} \leq m+\ell\cdot(n) -1$ then, it can be written as  $\left(h_n\right)_{t}^{t+\ell-1}(x)\cdot r_1(x) \cdot x^{t-m}$
	\begin{alignat*}{2}
		h_{n-1}(x) &= \left(h_n\right)_0^{t-1}(x) + \left(h_n\right)_{t}^{t+\ell-1}(x)\cdot r_1(x) \cdot x^{t-m}\\
		&\equiv \left(h_n\right)_0^{t-1}(x)+ \left(h_n\right)_{t}^{t+\ell-1}(x)\cdot x^{m} \cdot x^{t-m} \mod{\irred}\\
		&=  \left(h_n\right)_0^{t-1}(x)+ \left(h_n\right)_{t}^{t+\ell-1}(x)\cdot x^{t}\\
		&= h_n(x)
	\end{alignat*}
	
	So we return $h_0(x)$ of degree at most $m-1$ the equivalent to $h_k(x) = a(x)\cdot b(x) $ modulo \irred, which proves the correctness of the algorithm.
\end{proof}


	\subsubsection{Finding a k-Gapped polynomial}
According to Lidl and Niederreiter ~\cite{lidl1997finite} the probability of a randomly chosen polynomial to be irreducible is roughly $\sfrac{1}{n}$.
In a paper published by HP ~\cite{Seroussi98tableof} it was mentioned that in binary extension fields,
it should be quite probable to find $k-\irredProperty$ pentanomials for $k$ s.t. ${k \choose 3}\approx m$, they also present a list of $k-\irredProperty$ pentanomials that satisfy this $k$ for any practical $m$. They also raise an open question whether do irreducible binary pentanomials exist of degree $n$ that are \(\Omega{\left(\sqrt[3]{n^2}\right)}\) gapped.

Notice that multiplying any polynomial of degree $m$ by a quadrinomial or pentanomial can be done in $O\left(m\right)$, so under the assumptions presented in ~\cite{Seroussi98tableof}, this is a modular reduction in finite fields with linear time complexity.

\subsection{Generalizing Gao \& Mateer's Additive FFT for affine subspaces}
The Additive FFT algorithm introduced by Gao and Matteer ~\cite{Gao:2010:AFF:1921985.1922010} evaluates a $2^n-1$  degree polynomial over a subspace of dimension $n$ for general $n$ in a finite field of characteristic two. It was the first algorithm that broke the $\Omega(n\cdot\log^2(n))$ multiplications barrier, with only $O(n\cdot\log(n))$ base field multiplications. See \cite{Mateer:2008:FFT:1559083} for previous FFT algorithms with the same runtime that were suited only for subspaces with dimensions which is a power of two.

We present a variation of that algorithm that fits affine subspaces as well.
For the sake of completeness, we will describe the whole algorithm, relying on formulations and notations used by Gao and Mateer in their paper mentioned above.

\subsubsection{Taylor Expansion}
The additive FFT algorithm computes at some points the generalized Taylor expansion of polynomials at $\left(x^2-x\right)$. A more general definition can be found at \cite{Gathen:2003:MCA:945759} and at \cite{Gao:2010:AFF:1921985.1922010}.

Given polynomial \(f(x)\in \mathbb{F}[x]\) of degree strictly smaller than $n=2^{k+2}$ where \(\mathbb{F}\) is a finite field of characteristic 2, the taylor expansion algorithm of $f$ at $\left(x^2-x\right)$ finds $m=\frac{n}{2}$ linear functions $h_0(x),h_1(x),...,h_{m-1}(x) \in \mathbb{F}[x]$, such that,
\begin{equation*}
	f(x)=h_0(x)+h_1(x)\cdot(x^2-x)+...+h_{m-1}\cdot(x^2-x)^{m-1}
\end{equation*}

We will denote this expansion as
\begin{equation*}
	T\left(f,n\right)=\left(h_0,...,h_{m-1}\right)
\end{equation*}

To compute the Taylor expansion, we first write $f(x)$ as $f(x) = f_0(x) + x^{2^{k+1}}\left(f_1(x)+x^{2^k}f_2(x)\right)$ where
\begin{itemize}
\item $\deg f_0 < 2^{k+1}$
\item $\deg f_1 < 2^{k}$
\item $\deg f_2 < 2^{k}$
\end{itemize}

$\mathbb{F}$ is a finite field of characteristic two, therefore,
\begin{equation*}
	x^{2^{k+1}}=(x^2-x)^{2^k}+x^{2^k}
\end{equation*}

thus
\begin{equation*}
	f(x) = f_0(x)+x^{2^k}\left(f_1(x)+f_2(x)\right) + (x^2-x)^{2^k}\left(f_1(x)+f_2(x)+x^{2^k}f_2(x)\right)
\end{equation*}

Let $h(x)=f_1(x)+f_2(x)$,\quad $g_0(x)=f_0(x)+x^{2^k}h_(x)$,\quad $g_1(x)=h(x)+x^{2^k}f_2(x)$.

Then,
\begin{equation*}
	f(x)=g_0(x)+g_1(x)(x^2-x)^{2^k}
\end{equation*}
Due to the degrees of $f_0,f_1,f_2$ we know that
\begin{equation*}
	\deg g_0,g_1 < 2^{k+1}
\end{equation*}

Therefore,
\begin{equation*}
	T(f,n)=\left(T(\left(g_0,2^{k+1}\right)),T\left(g_1,2^{k+1}\right)\right)
\end{equation*}

The time complexity of the algorithm, as described in \cite{Gao:2010:AFF:1921985.1922010} is,
\begin{equation}
\left\{
	\begin{array}{l l}
	\leq n\cdot \left\lceil \log_2(n/t)\right\rceil, & \text{for any} \ n \\
	= \frac{1}{2}n\left\lceil \log_2(n/t)\right\rceil, & \text{when $n/t$ is a power of two}
	\end{array}
	\right.
\end{equation}
A full description of the algorithm can be found at \autoref{alg:taylor}.

\begin{algorithm}
	\caption{Taylor Expansion at $x^2-x$}
	\label{alg:taylor}
	\begin{algorithmic}[1]
		\Require $(f,n)$ where $n\geq 1$ and $f(x)\in \mathbb{F}[x]$ of degree $<n$.
		\Ensure  $T(f,n)$, the taylor expansion of $f(x)$ at $x^2-x$.
		\If{$n\leq 2$}
		\State \Return $f(x)$
		\EndIf
		\State Find $k$ such that $2^{k+1} < n \leq 2^{k+2}$.
		\State Divide $f(x)$ into three parts as $f(x) = f_0(x)+x^{2^k+1}\left(f_1(x)+x^{2^k}f_2(x)\right)$
		\State Set $h\leftarrow f_1+f_2, \quad g_0\leftarrow f_0+x^{2^k}h, \quad g_1\leftarrow h+x^{2^k}f_2.$
		\State $V_1 \leftarrow T(g_0,n/2)$
		\State $V_2 \leftarrow T(g_1,n/2)$
		\State \Return $\left(V_1,V_2\right)$
	\end{algorithmic}
\end{algorithm}

\subsubsection{Additive FFT in Binary Fields Over Affine Subspaces}
In this section we will conform with the notations of Gao and Mateer and extended their algorithm presented in \cite{Gao:2010:AFF:1921985.1922010}.

Our additive FFT algorithm works over a finite field $\mathbb{F}$ of characteristic 2. It gets as input a polynomial $f(x) \in \mathbb{F}[x]$, a basis of a subspace $\left\langle \beta_1,\ldots,\beta_m \right\rangle$ of dimension $m$, where $\beta_1,...,\beta_m$ are linearly independent over \GF{}, it also gets as input an affine shift $s_B$.


Let us define an order on the elements of $B$. Given a number $0\leq i<2^m$ with binary representation
\begin{equation*}
	i=a_1+a_2\cdot 2 + \cdots + a_m\cdot 2^{m-1} = (a_1,a_2,\cdots,a_m)_2,
\end{equation*}
Where each $a_j$ is either 0 or 1.
The $i^{th} $ element of affine subspace $B$ is
\begin{equation*}
	B[i]=s+a_1\beta_1+a_2\beta_2+\cdots+a_m\beta_m.
\end{equation*}

The algorithm's output is the evaluation of $f(x)$ over all elements in the affine subspace $B=s_B+\left\langle \beta_1,...,\beta_m \right\rangle$, and will be denoted as,
\begin{equation*}
	FFT(f,m,B)=\left(f\left(B[0]\right),f\left(B[1]\right),\cdots,f\left(B[2^m-1]\right)\right)
\end{equation*}

The algorithm is recursive, we show how to reduce a problem of size $n>2$ to two problems of size $k=n/2=2^{m-1}$. Let
\begin{equation*}
\begin{array}{l}
	\gamma_i=\beta_i\cdot\beta_m^{-1}, \quad 1\leq i \leq m-1 \\
	s_G=s_B\cdot \beta_m^{-1}
\end{array}
\end{equation*}
and
\begin{alignat}{3}
	G &= s_G+\left\langle \gamma_1,...,\gamma_m \right\rangle
\end{alignat}

Let $g(x)=f(\beta_mx)$. Evaluating $f(x)$ over $B$ is equivalent to the evaluation of $g(x)$ over $G \cup (G+1)$. So we wish to calculate FFT$(g,G)$ and FFT$(g,(G+1))$. Let $D=s_D+\left\langle \delta_1,\dots,\delta_{m-1}\right\rangle$ where,
\begin{alignat*}{2}
	\delta_i=\gamma_i^2-\gamma_i, & \quad  1\leq i \leq m-1
\end{alignat*}

We know that each $\gamma_i$ is not 1 or 0, so $\delta_i$ is not 0. Since $\gamma_1,...,\gamma_m $ and 1 are linearly independent over \GF{} the elements $\delta_1,\dots,\delta_{m-1}$ are linearly independent over \GF{} as well and span the affine subspace,
\begin{equation*}
	D = s_D+\left\langle \delta_1,\dots,\delta_{m-1}\right\rangle
\end{equation*}
of size $k=2^{m-1}=n/2$.
\begin{notation}
	Given $\alpha=a_1\gamma_1+\cdots+a_{m-1}\gamma_{m-1}\in G$, the element $\alpha^*$ is
	\begin{equation*}
		\alpha^*=\alpha^2-\alpha = a_1\delta_1+\cdots + a_{m-1}\delta_{m-1}
	\end{equation*}
\end{notation}
Therefore,
\begin{equation*}
	G[i]^*=D[i], \quad 0\leq i < k
\end{equation*}

Suppose we are given the Taylor expansion of $g(x)$ at $x^2-x$.
\begin{equation}
	g(x)=\sum_{i=0}^{k-1}\left(g_{i0}+g_{i1}x\right)\cdot\left(x^2-x\right)^i
\end{equation}
and $g_{ij}\in \mathbb{F}$. Let
\begin{equation}
\label{partition}
	g_0(x)=\sum_{i=0}^{k-1}g_{i0}\cdot x^i, \quad \textrm{and} \quad g_1(x)=\sum_{i=0}^{k-1}g_{i1}\cdot x^i.
\end{equation}

Notice that for any $\alpha \in G$ and $b\in \GF{}$, since $(\alpha+b)^2-(\alpha+b)=\alpha^*$, we have
\begin{equation}
\label{eq:fromgtod}
	g(\alpha+b)=\left(g_0\left(\alpha^*\right)+\alpha\cdot g_1\left(\alpha^*\right)\right)+bg_1\left(\alpha^*\right)
\end{equation}
Therefore, the FFT of $g(x)$ can be calculated from the FFTs of $g_0(x)$ and $g_1(x)$ over $D$. Let the FFT of $g_0(x)$ and $g_1(x)$ over $D$ be,
\begin{equation}
\begin{array}{l r}
	FFT(g_0,m-1,D) = \left(u_0,u_1,\dots,u_{k-1}\right), & u_i=g_0(D[i])	\\
	FFT(g_1,m-1,D) = \left(v_0,v_1,\dots,v_{k-1}\right), & v_i=g_1(D[i])
\end{array}
\end{equation}
\autoref{eq:fromgtod} implies that
\begin{equation*}
	FFT(g,m-1,G)=\left(w_0,w_1,\dots,w_{k-1}\right)
\end{equation*}
Where $w_i=u_i+G[i]\cdot u_i$ for $0\leq i < k$. It also implies that,
\begin{equation*}
	FFT(g,m-1,G+1) = FFT(g,m-1,G)+FFT(g_1,m-1,D).
\end{equation*}
This reduction step is applied recursively until the input polynomials are linear functions that can be evaluated easily. In \autoref{alg:fft} a summary of the written above can be found.


\begin{algorithm}
	\caption{Additive FFT of length $n=2^m$}
	\label{alg:fft}
	\begin{algorithmic}[1]
		\Require
		\Statex \begin{itemize}
			\item $f(x)\in \GF{t}[X] $ of degree \(< n=2^m\).
			\item $B= \langle \beta_1,...,\beta_m \rangle $, a basis with linearly independent elements over \GF{}.
			\item $S$, an affine shift to the subspace spanned by \(\beta_i\)'s.
		\end{itemize}
		\Ensure \(FFT(f,m,B,S) = \left(f(B[0]+S),...,f(B[n-1]+S)\right)\)
		\State If $m=1$ then return \(f(S),f(S+\beta_1)\).\label{step:bottomFFT}
		\Comment Linear Evaluation Phase.
		\State Compute $g(x) = f\left(\beta_m x\right)$.
		\Comment Shift Phase.
		\label{step:multiexp}
		\State Compute the Taylor expansion of $g(x)$ as in \autoref{alg:taylor}
		\Comment Taylor Expansion Phase.
		\State Let $g_0(x)$ and $g_1(x)$ from $g(x)$ as in (\ref{partition}).
		\Comment Shuffle Phase.\label{itm:shuffle}
		\State Compute $\gamma_i\leftarrow \beta_i\cdot \beta_m^{-1}$, \quad $\delta_i\leftarrow \gamma_i^2-\gamma_i$ for $1\leq i < m$, \quad
				$s_G\leftarrow S\cdot\beta_m^{-1}$ and $s_D\leftarrow s_G^2-s_G$.\label{step:subspaces}
		\State Let $G\leftarrow s_G+\left\langle\gamma_1,\dots,\gamma_{m-1}\right\rangle$, and $D\leftarrow \left\langle\delta_1,\dots,\delta_{m-1}\right\rangle$.
		\State {Let $k=2^{m-1}$ compute \par
			FFT$(g_0,m-1,D,s_D)=(u_0,u_1,\dots,u_{k-1})$, and \par
			FFT$(g_1,m-1,D,s_D)=(v_0,v_1,\dots,v_{k-1})$}. \label{step:fft_recursive}
		\For{$0\leq i < k$}
			\State $w_i\leftarrow u_i+G[i]\cdot v_i$
			\State $w_{k+1}\leftarrow w_i+v_i$.
			\label{step:WFromUV}
			\Comment Merge Phase.
		\EndFor
		\State \Return $\left(w_0,w_1,\dots,w_{n-1}\right)$
		\end{algorithmic}
	\end{algorithm}

The only two additions we made to Gao and Mateer's algorithm is calculating recursively a series of affine shifts. The only place which these shifts take place is the bottom of the recursion, where we evaluate the linear function as described in step \ref{step:bottomFFT}.

We will now compute the runtime of the algorithm. To compute the basis elements of $G$ and $D$ and the affine shifts in step \ref{step:subspaces}, we perform $2m+2(m-1)+\cdots+2\cdot 2 =m(m+1) = O(\log_2^2(n))$ multiplications, and the number of additions is $m+(m-1)+\cdots+2=m(m-1)/2=O(\log_2^2(n))$. In step \ref{step:multiexp}, we compute the powers of $\beta_m^i$ for $2\leq i \leq n-1$, with a total number of multiplications that is at most $(2^m-2)+(2^{m-1}-2)+\cdots+(2^2-2)<2\cdot2^m=2n$.
Up until now, the whole computations can be preprocessed, and either way, cost negligible time.

In step \ref{step:bottomFFT} the recursions ends and it costs 2 multiplications and 2 additions.
Step \ref{step:multiexp} costs additional $n-1$ multiplications (besides computing the powers of $\beta_m$). The Taylor expansions costs additional $\frac{1}{2}\cdot n \cdot \log_2(n)-\frac{1}{2}\cdot n$ additions.
Step \ref{step:fft_recursive} is two invocations of the FFT algorithm of size $n/2$. Step \ref{step:WFromUV} costs $n$ multiplications and $n$ additions.
Let $M(n)$ and $A(n)$ denote the number of multiplications and additions performed by the algorithm, respectively, on an input of size $n$. Then $M(2)=A(2)=2$, and for any $n=2^m>2$, it holds that
\begin{displaymath}
\begin{array}{r c l}
	M(n) &= & 2\cdot M\left(\frac{n}{2}\right)+2n-1, \\
	A(n) &= & 2\cdot A\left(\frac{n}{2}\right)+\frac{1}{2}\cdot n \cdot \log_2(n)+\frac{1}{2}\cdot n.
\end{array}
\end{displaymath}

By induction we get

\begin{displaymath}
\begin{array}{r c l}
M(n) &= & 2\cdot n\cdot \log_2(n)-\frac{3n}{2}+1, \\
A(n) &= & \frac{1}{4}\cdot n \cdot \left(\log_2(n)\right)^2+\frac{3}{4}\cdot n \cdot \log_2(n)
\end{array}
\end{displaymath}
	
	
\subsection{Parallel FFT and inverse FFT implementation}
A new implementation for the FFT and inverse FFT algorithms is presented. Implemented using openMP.
Unfortunately, despite being extremely fast for a single thread, the implementation didn't scale-up for large number of cores due to excessive cache misses and cache coherency protocols bandwidth.
Most cache misses occured in the Taylor expansion implementation in which accesses to the array of elements were very strided. Two possible ways of avoiding this problem is either working with manually managed cache (e.g. GPU) or finding another algorithm for the Taylor expansion that would be more cache-friendly. Another reason to cause this lack of scalability is the NUMA (non-uniform memory access) in which the memory is partitioned into segments. Each segment is physically located close to one CPU, and is considered its' local memory. Each CPU can accesses its' own local memory very quickly, but can also access other CPUs local memory. In that case, the accesses takes more time and the data is crosses through a bus with small bandwidth which limits the scalability of our implementation.


